module FixtureFileHelper
  def fixture_file_content(path, data = {})
    content = IO.read(fixture_file(path))
    return content unless path.end_with?('.erb')

    ERB
      .new(content)
      .result(OpenStruct.new(data).instance_eval { binding })
  end

  def fixture_file(path)
    License::Management.root.join("spec/fixtures/#{path}")
  end
end
