RSpec::Matchers.define :match_schema do |version: License::Management::Report::DEFAULT_VERSION|
  def schema_for(version)
    License::Management.root.join("spec/fixtures/schema/v#{version}.json").to_s
  end

  match do |actual|
    !actual.nil? && (@errors = JSON::Validator.fully_validate(schema_for(version), actual.to_h)).empty?
  end

  failure_message do |response|
    "didn't match the schema for version #{version}" \
    " The validation errors were:\n#{@errors.join("\n")}"
  end
end
