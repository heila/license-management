RSpec.describe License::Management::Repository do
  describe "#item_for" do
    let(:spdx_licenses) { JSON.parse(IO.read('spdx-licenses.json'))['licenses'] }

    context "when mapping a license that refers to opensource.org" do
      it 'parses the SPDX id from the url' do
        spdx_licenses.each do |license|
          spdx_id = license['licenseId']
          url = "https://opensource.org/licenses/#{spdx_id}"
          license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)
          expect(subject.item_for(license)['id']).to eql(spdx_id)
        end
      end

      it 'recognizes `http://www.opensource.org/licenses/mit-license.php`' do
        url = 'http://www.opensource.org/licenses/mit-license.php'
        license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)

        expect(subject.item_for(license)['id']).to eql('MIT')
      end
    end

    context "when mapping a license that refers to nuget.org" do
      it 'parses the SPDX id from the url' do
        spdx_licenses.each do |license|
          spdx_id = license['licenseId']
          url = "https://licenses.nuget.org/#{spdx_id}"
          license = LicenseFinder::License.new(short_name: url, matcher: LicenseFinder::License::NoneMatcher.new, url: url)
          expect(subject.item_for(license)['id']).to eql(spdx_id)
        end
      end
    end

    [
      ['Apache License v2.0', 'Apache-2.0']
    ].each do |short_name, spdx_id|
      context "when mapping a `#{short_name}` license" do
        let(:license) { LicenseFinder::License.new(short_name: short_name, matcher: LicenseFinder::License::NoneMatcher.new, url: nil) }
        let(:dependency) { double(name: 'x', summary: '', description: '', homepage: '', licenses: [license]) }

        it { expect(subject.item_for(license)['id']).to eql(spdx_id) }
      end
    end
  end
end
