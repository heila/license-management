RSpec.describe License::Management::Report::V2_1 do
  describe "#to_h" do
    context "when choosing an appropriate url for a license" do
      subject { described_class.new([rails, spandx, dotenv_js, dotenv_rb]).to_h }

      let(:rails) { LicenseFinder::Dependency.new('Bundler', 'rails', '0.1.0', spec_licenses: ['MIT']) }
      let(:spandx) { LicenseFinder::Dependency.new('Bundler', 'spandx', '0.1.0', spec_licenses: ['Apache-2.0', 'MIT']) }
      let(:dotenv_js) { LicenseFinder::Dependency.new('Npm', 'dotenv', '0.1.0', spec_licenses: ['MIT']) }
      let(:dotenv_rb) { LicenseFinder::Dependency.new('Bundler', 'dotenv', '0.1.0', spec_licenses: ['MIT']) }

      specify { expect(subject[:version]).to eql('2.1') }

      specify do
        expect(subject[:licenses]).to match_array([
          {
            'id' => 'Apache-2.0',
            'name' => 'Apache License 2.0',
            'url' => 'https://opensource.org/licenses/Apache-2.0'
          },
          {
            'id' => 'MIT',
            'name' => 'MIT License',
            'url' => 'https://opensource.org/licenses/MIT'
          }
        ])
      end

      specify do
        expect(subject[:dependencies]).to match_array([
          { name: dotenv_js.name, version: dotenv_js.version, package_manager: :npm, path: '.', licenses: ['MIT'] },
          { name: dotenv_rb.name, version: dotenv_rb.version, package_manager: :bundler, path: '.', licenses: ['MIT'] },
          { name: rails.name, version: rails.version, package_manager: :bundler, path: '.', licenses: ['MIT'] },
          { name: spandx.name, version: spandx.version, package_manager: :bundler, path: '.', licenses: ['Apache-2.0', 'MIT'] }
        ])
      end
    end
  end
end
