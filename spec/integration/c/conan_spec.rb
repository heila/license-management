require 'spec_helper'

RSpec.describe "conan" do
  include_examples "each report version", "c", "conan"

  context "when scanning a C++ project" do
    subject { runner.scan(env: env) }

    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--project-path=libraries/poco/md5' } }

    before do
      runner.clone('https://github.com/conan-io/examples.git')
    end

    specify { expect(subject).to match_schema }
    specify { expect(subject.dependency_names).to match_array(%w[openssl poco]) }
    specify { expect(subject.licenses_for('openssl')).to match_array(['OpenSSL']) }
    specify { expect(subject.licenses_for('poco')).to match_array(['BSL-1.0']) }
  end

  context "when scanning a folly project" do
    subject { runner.scan(env: env) }

    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--project-path=libraries/folly/basic' } }

    before do
      runner.clone('https://github.com/conan-io/examples.git')
    end

    specify { expect(subject).to match_schema }
    specify { expect(subject.licenses_for('boost')).to match_array(['BSL-1.0']) }
    specify { expect(subject.licenses_for('bzip2')).to match_array(['bzip2-1.0.8']) }
    specify { expect(subject.licenses_for('double-conversion')).to match_array(['BSD-3-Clause']) }
    specify { expect(subject.licenses_for('folly')).to match_array(['Apache-2.0']) }
    specify { expect(subject.licenses_for('gflags')).to match_array(['BSD-3-Clause']) }
    specify { expect(subject.licenses_for('glog')).to match_array(['BSD-3-Clause']) }
    specify { expect(subject.licenses_for('libdwarf')).to match_array(['LGPL-2.1']) }
    specify { expect(subject.licenses_for('libelf')).to match_array(['LGPL-2.0']) }
    specify { expect(subject.licenses_for('libevent')).to match_array(['BSD-3-Clause']) }
    specify { expect(subject.licenses_for('libiberty')).to match_array(['LGPL-2.1']) }
    specify { expect(subject.licenses_for('libsodium')).to match_array(['ISC']) }
    specify { expect(subject.licenses_for('libunwind')).to match_array(['MIT']) }
    specify { expect(subject.licenses_for('lz4')).to match_array(['BSD-2-Clause', 'BSD-3-Clause']) }
    specify { expect(subject.licenses_for('openssl')).to match_array(['OpenSSL']) }
    specify { expect(subject.licenses_for('snappy')).to match_array(['BSD-3-Clause']) }
    specify { expect(subject.licenses_for('zlib')).to match_array(['Zlib']) }
    specify { expect(subject.licenses_for('zstd')).to match_array(['BSD-3-Clause']) }
  end

  context "when scanning a project with cmake" do
    subject { runner.scan(env: env) }

    let(:env) { { 'LICENSE_FINDER_CLI_OPTS' => '--project-path=libraries/protobuf/serialization' } }

    before do
      runner.clone('https://github.com/conan-io/examples.git')
    end

    specify { expect(subject).to match_schema }
    specify { expect(subject.dependency_names).to match_array(%w[protobuf protoc_installer]) }
    specify { expect(subject.licenses_for('protobuf')).to match_array(['BSD-3-Clause']) }
    specify { expect(subject.licenses_for('protoc_installer')).to match_array(['BSD-3-Clause']) }
  end
end
