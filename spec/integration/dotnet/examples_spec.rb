require 'spec_helper'

RSpec.describe ".NET Core" do
  it 'scans https://github.com/microsoft/RockPaperScissorsLizardSpock.git' do
    runner.clone('https://github.com/microsoft/RockPaperScissorsLizardSpock.git')
    report = runner.scan(env: { 'LICENSE_FINDER_CLI_OPTS' => '--recursive' })

    expect(report).to match_schema
    expect(report[:licenses].count).not_to be_zero
    expect(report[:dependencies].count).not_to be_zero
  end
end
