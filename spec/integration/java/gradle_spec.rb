require 'spec_helper'

RSpec.describe "gradle" do
  include_examples "each report version", "java", "gradle"

  context "when running a default gradle build" do
    it 'scans a gradle project' do
      content = <<~GRADLE
/*
 * This file was generated by the Gradle 'init' task.
 *
 * This is a general purpose Gradle build.
 * Learn how to create Gradle builds at https://guides.gradle.org/creating-new-gradle-builds
 */
plugins {
  id "com.github.hierynomus.license" version "0.15.0"
}
      GRADLE
      runner.add_file('build.gradle', content)

      report = runner.scan
      expect(report).to match_schema
      expect(report[:licenses]).to be_empty
      expect(report[:dependencies]).to be_empty
    end
  end

  context 'when scanning a gradle project that does not include the `com.github.hierynomus.license` plugin' do
    let(:project_url) { 'https://gitlab.com/one-touch-pipeline/otp.git' }
    let(:result) { runner.scan }

    before do
      runner.clone(project_url)
    end

    it 'is able to detect licenses' do
      expect(result).to match_schema
      expect(result[:licenses]).not_to be_empty

      [
        { name: 'ant', licenses: ['Apache-2.0'] },
        { name: 'activation', licenses: ['CDDL-1.0'] },
        { name: 'xml-apis', licenses: ['Apache-2.0', 'SAX-PD', 'W3C-20150513'] },
        { name: 'sitemesh', licenses: ['Apache-1.1'] },
        { name: 'hibernate-jpa-2.1-api', licenses: ['BSD-3-Clause', 'EPL-1.0'] }
      ].each do |dependency|
        expect(result.licenses_for(dependency[:name])).to match_array(dependency[:licenses])
      end
    end
  end

  context 'when scanning a project that needs to connect to multiple TLS endpoints with different custom certificate chains' do
    subject do
      runner.scan(env: {
        'ADDITIONAL_CA_CERT_BUNDLE' => fixture_file_content('java/gradle/offline-environment/bundle.crt'),
        'PRIVATE_MAVEN_HOST' => private_maven_host
      })
    end

    before do
      runner.mount(dir: fixture_file('java/gradle/offline-environment/'))
    end

    specify { expect(subject).to match_schema }

    specify do
      expect(subject.dependency_names).to match_array([
        "antlr",
        "commons-beanutils",
        "commons-io",
        "commons-lang",
        "commons-lang3",
        "fastutil",
        "findbugs-annotations",
        "geode-common",
        "geode-core",
        "geode-json",
        "jackson-annotations",
        "jackson-core",
        "jackson-databind",
        "javax.resource-api",
        "javax.transaction-api",
        "jgroups",
        "jna",
        "jopt-simple",
        "log4j-api",
        "log4j-core",
        "maven-artifact",
        "netty",
        "plexus-utils",
        "rhino",
        "shiro-core",
        "slf4j-api"
      ])
    end
  end

  context "when scanning a gradle project with a custom option to generate a profiler report" do
    let(:report) { runner.scan(env: { 'GRADLE_CLI_OPTS' => '--profile' }) }

    before do
      runner.add_file('build.gradle', fixture_file_content("java/11/build.gradle"))
    end

    specify { expect(report).to match_schema }
    specify { expect { report }.to change { Dir.glob("#{runner.project_path}/build/reports/profile/profile-*.html").count }.from(0).to(1) }
    specify { expect(report.dependency_names).to match_array(['postgresql']) }
    specify { expect(report.licenses_for('postgresql')).to match_array(['BSD-2-Clause']) }
  end

  context 'when using Java 8 with version 1.* of gradle' do
    before do
      runner.add_file('.tool-versions', "gradle 1.9")
      runner.add_file('build.gradle', fixture_file_content("java/8/build.gradle"))
    end

    it 'returns an empty report because the plugin we use does not work in this version of the gradle API' do
      report = runner.scan(env: { 'LM_JAVA_VERSION' => '8' })

      expect(report).to match_schema
      expect(report[:dependencies]).to be_empty
      expect(report[:licenses]).to be_empty
    end
  end

  ['4.9', '5.6', '6.3'].each do |gradle_version|
    context "when using Java v11 with a kotlin project using gradle v#{gradle_version}" do
      let(:report) { runner.scan(env: { 'LM_JAVA_VERSION' => '11', 'GRADLE_CLI_OPTS' => '-b build.gradle.kts' }) }

      before do
        runner.add_file('.tool-versions', "gradle #{gradle_version}")
        runner.add_file('build.gradle.kts', fixture_file_content("java/build.gradle.kts"))
        runner.add_file('settings.gradle.kts', 'rootProject.name = "example"')
      end

      specify { expect(report).to match_schema }
      specify { expect(report.dependency_names).to match_array(['postgresql']) }
      specify { expect(report.licenses_for('postgresql')).to match_array(['BSD-2-Clause']) }
    end
  end

  [
    { java: '8', gradle: ['2.14', '3.5'] },
    { java: '11', gradle: ['4.9', '5.6', '6.3'] }
  ].each do |item|
    item[:gradle].each do |gradle_version|
      context "when using Java v#{item[:java]} with a gradle v#{gradle_version} on a groovy project" do
        let(:report) { runner.scan(env: { 'LM_JAVA_VERSION' => item[:java] }) }

        before do
          runner.add_file('.tool-versions', "gradle #{gradle_version}")
          runner.add_file('build.gradle', fixture_file_content("java/#{item[:java]}/build.gradle"))
          runner.add_file('settings.gradle', 'rootProject.name = "example"')
        end

        specify { expect(report).to match_schema }
        specify { expect(report.dependency_names).to match_array(['postgresql']) }
        specify { expect(report.licenses_for('postgresql')).to match_array(['BSD-2-Clause']) }
      end
    end
  end
end
