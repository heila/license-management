require 'spec_helper'

RSpec.describe "maven" do
  include_examples "each report version", "java", "maven"
  include_examples "each report version", "java", "maven-multimodules"

  describe "When the maven dependencies come from a custom public maven repository" do
    it 'is able to detect some of the licenses' do
      runner.add_file('pom.xml', fixture_file_content('java/pom-public-gitlab-repository.xml'))

      report = runner.scan(env: {
        'CI_PROJECT_ID' => '6130122'
      })

      expect(report).to match_schema
      expect(report.dependency_names).to match_array(['example'])
      expect(report.licenses_for('example')).to match_array(['MIT'])
    end

    it 'downloads packages from by using a custom `settings.xml`' do
      runner.add_file('pom.xml', fixture_file_content('java/pom-public-gitlab-repository.xml'))
      runner.add_file('my_settings.xml', fixture_file_content('java/custom-maven-settings.xml'))

      report = runner.scan(env: {
        'CI_PROJECT_ID' => 'invalid',
        'MAVEN_CLI_OPTS' => "--settings my_settings.xml"
      })

      expect(report).to match_schema
      expect(report[:dependencies]).to match_array([{ name: 'example', url: '', description: '', paths: ['.'], licenses: ['MIT'] }])
    end
  end

  describe "When using the `SETUP_CMD`" do
    it 'executes the custom script' do
      runner.add_file('custom.sh') do
        <<~SCRIPT
        #!/bin/bash -l

        echo 'hello'
        SCRIPT
      end

      report = runner.scan(env: {
        'SETUP_CMD' => 'bash custom.sh'
      })

      expect(report).to match_schema
    end
  end

  describe "When scanning a project with multiple modules" do
    before do
      runner.mount(dir: fixture_file('java/maven-multimodule'))
    end

    it 'detects dependences from each module' do
      report = runner.scan

      expect(report).to match_schema
      expect(report[:dependencies]).not_to be_empty

      [
        { name: "asm", licenses: ["BSD-4-Clause"] },
        { name: "asm-commons", licenses: ["BSD-4-Clause"] },
        { name: "jackson-annotations", licenses: ["Apache-2.0"] },
        { name: "jackson-core", licenses: ["Apache-2.0"] },
        { name: "jackson-databind", licenses: ["Apache-2.0"] },
        { name: "jackson-dataformat-xml", licenses: ["Apache-2.0"] },
        { name: "jackson-module-jaxb-annotations", licenses: ["Apache-2.0"] },
        { name: "log4j-api", licenses: ["Apache-2.0"] },
        { name: "log4j-core", licenses: ["Apache-2.0"] },
        { name: "netty-all", licenses: ["Apache-2.0"] },
        { name: "stax2-api", licenses: ["BSD-4-Clause"] }
      ].each do |dependency|
        expect(report.licenses_for(dependency[:name])).to match_array(dependency[:licenses])
      end

      expect(report.dependency_names).not_to include('junit')
    end
  end

  context "when connecting to a custom package registry with a self signed certificate" do
    let(:bundle) { fixture_file_content('java/maven.crt') }
    let(:report) { runner.scan(env: { 'ADDITIONAL_CA_CERT_BUNDLE' => bundle, 'LOG_LEVEL' => 'debug' }) }

    before do
      runner.add_file('pom.xml') do
        fixture_file_content('java/pom-single.xml.erb', {
          group_id: 'com.fasterxml.jackson.core',
          artifact_id: 'jackson-core',
          version: '2.10.0',
          repository_id: 'custom',
          repository_url: "https://#{private_maven_host}/artifactory/mvn-cache"
        })
      end
    end

    specify { expect(report).to match_schema }
    specify { expect(report.dependency_names).to match_array(['jackson-core']) }
    specify { expect(report.licenses_for('jackson-core')).to match_array(['Apache-2.0']) }
  end
end
