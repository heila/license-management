require 'spec_helper'

RSpec.describe "composer" do
  include_examples "each report version", "php", "composer"

  context "when the project's dependencies require php-gd e.g. in the case of Drupal" do
    it 'installs the required dependencies and produces a valid report' do
      # composer.json from https://git.drupalcode.org/project/drupal/raw/8.7.x/core/composer.json
      runner.add_file('composer.json', fixture_file_content('php/drupal_composer.json'))

      report = runner.scan

      expect(report).to match_schema
      expect(report[:version]).not_to be_empty
      expect(report[:licenses]).not_to be_empty
      expect(report.dependency_names).to match_array(%w[
        asm89/stack-cors
        behat/mink
        behat/mink-browserkit-driver
        behat/mink-goutte-driver
        behat/mink-selenium2-driver
        brumann/polyfill-unserialize
        composer/semver
        doctrine/annotations
        doctrine/cache
        doctrine/collections
        doctrine/common
        doctrine/event-manager
        doctrine/inflector
        doctrine/instantiator
        doctrine/lexer
        doctrine/persistence
        doctrine/reflection
        drupal/coder
        easyrdf/easyrdf
        egulias/email-validator
        fabpot/goutte
        guzzlehttp/guzzle
        guzzlehttp/promises
        guzzlehttp/psr7
        instaclick/php-webdriver
        jcalderonzumba/gastonjs
        jcalderonzumba/mink-phantomjs-driver
        justinrainbow/json-schema
        masterminds/html5
        mikey179/vfsstream
        myclabs/deep-copy
        paragonie/random_compat
        pear/archive_tar
        pear/console_getopt
        pear/pear-core-minimal
        pear/pear_exception
        phar-io/manifest
        phar-io/version
        phpdocumentor/reflection-common
        phpdocumentor/reflection-docblock
        phpdocumentor/type-resolver
        phpspec/prophecy
        phpunit/php-code-coverage
        phpunit/php-file-iterator
        phpunit/php-text-template
        phpunit/php-timer
        phpunit/php-token-stream
        phpunit/phpunit
        phpunit/phpunit-mock-objects
        psr/container
        psr/http-message
        psr/log
        ralouphie/getallheaders
        sebastian/code-unit-reverse-lookup
        sebastian/comparator
        sebastian/diff
        sebastian/environment
        sebastian/exporter
        sebastian/global-state
        sebastian/object-enumerator
        sebastian/object-reflector
        sebastian/recursion-context
        sebastian/resource-operations
        sebastian/version
        squizlabs/php_codesniffer
        stack/builder
        symfony-cmf/routing
        symfony/browser-kit
        symfony/class-loader
        symfony/console
        symfony/css-selector
        symfony/debug
        symfony/dependency-injection
        symfony/dom-crawler
        symfony/event-dispatcher
        symfony/http-foundation
        symfony/http-kernel
        symfony/phpunit-bridge
        symfony/polyfill-ctype
        symfony/polyfill-iconv
        symfony/polyfill-intl-idn
        symfony/polyfill-mbstring
        symfony/polyfill-php56
        symfony/polyfill-php70
        symfony/polyfill-php72
        symfony/polyfill-util
        symfony/process
        symfony/psr-http-message-bridge
        symfony/routing
        symfony/serializer
        symfony/translation
        symfony/validator
        symfony/yaml
        theseer/tokenizer
        twig/twig
        typo3/phar-stream-wrapper
        webmozart/assert
        zendframework/zend-diactoros
        zendframework/zend-escaper
        zendframework/zend-feed
        zendframework/zend-stdlib
      ])
    end
  end
end
