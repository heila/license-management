require 'spec_helper'

RSpec.describe "modules" do
  include_examples "each report version", "go", "modules"

  ['1.11', '1.12', '1.13', '1.14'].each do |version|
    context "when scanning a go.mod and go.sum files with v#{version}" do
      subject { runner.scan }

      before do
        runner.add_file('main.go', fixture_file_content('go/main.go'))
        runner.add_file('go.mod', fixture_file_content('go/go.mod'))
        runner.add_file('go.sum', fixture_file_content('go/go.sum'))
        runner.add_file('.tool-versions', "golang #{version}")
      end

      specify { expect(subject).to match_schema }
      specify { expect(subject[:licenses]).not_to be_empty }

      specify do
        expect(subject.dependency_names).to match_array([
          "github.com/davecgh/go-spew",
          "github.com/dimfeld/httptreemux/v5",
          "github.com/golang/protobuf",
          "github.com/go-logfmt/logfmt",
          "github.com/google/uuid",
          "github.com/pmezard/go-difflib",
          "github.com/stretchr/objx",
          "github.com/stretchr/testify",
          "golang.org/x/net",
          "golang.org/x/oauth2",
          "golang.org/x/sync",
          "google.golang.org/appengine",
          "gopkg.in/check.v1",
          "gopkg.in/yaml.v2"
        ])
      end

      specify { expect(subject.licenses_for('github.com/dimfeld/httptreemux/v5')).to match_array(['MIT']) }
      specify { expect(subject.licenses_for('github.com/go-logfmt/logfmt')).to match_array(['MIT']) }
      specify { expect(subject.licenses_for('github.com/google/uuid')).to match_array(['BSD-3-Clause']) }
      specify { expect(subject.licenses_for('github.com/stretchr/testify')).to match_array(['MIT']) }
      specify { expect(subject.licenses_for('golang.org/x/oauth2')).to match_array(['BSD-3-Clause']) }
    end
  end

  context "when scanning the `gitaly` project" do
    subject { runner.scan }

    before do
      runner.clone('https://gitlab.com/gitlab-org/gitaly.git')
    end

    specify { expect(subject).to match_schema }
    specify { expect(subject[:licenses]).not_to be_empty }
    specify { expect(subject[:dependencies]).not_to be_empty }
  end
end
