# frozen_string_literal: true

module LicenseFinder
  class NPM
    def current_packages
      NpmPackage.packages_from_json(npm_json, detected_package_path).map do |item|
        Dependency.from(item, detected_package_path)
      end
    end

    def prepare_command
      lockfile? ? 'npm ci' : 'npm install --no-save'
    end

    def possible_package_paths
      [project_path.join('package.json')]
    end

    def prepare
      Dir.chdir(project_path) do
        shell.execute("#{prepare_command} --production", env: default_env)
      end
    end

    private

    def lockfile?
      File.exist?(project_path.join('package-lock.json'))
    end

    def npm_json
      stdout, _stderr, status = Dir.chdir(project_path) do
        shell.execute("npm list --json --long --production")
      end
      status.success? ? JSON.parse(stdout) : {}
    end

    def default_env
      return {} unless shell.custom_certificate_installed?

      { 'NPM_CONFIG_CAFILE' => ENV.fetch('NPM_CONFIG_CAFILE', shell.custom_certificate_path.to_s) }
    end
  end
end
