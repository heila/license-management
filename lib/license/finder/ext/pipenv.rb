# frozen_string_literal: true

module LicenseFinder
  class Pipenv
    def prepare
      return unless pipfile?

      shell.execute([:pipenv, '--python', python.version], env: python.default_env)
      shell.execute([:pipenv, :run, :pipenv, :sync, '--pypi-mirror', python.pip_index_url], env: python.default_env)
    end

    def current_packages
      return legacy_results unless pipfile?

      python.pip_licenses(detection_path: detected_package_path)
    end

    private

    def python
      @python ||= ::License::Management::Python.new
    end

    def pipfile?
      detected_package_path.dirname.join('Pipfile').exist?
    end

    def legacy_results
      packages = {}
      each_dependency(groups: allowed_groups) do |name, data, group|
        version = canonicalize(data['version'])
        package = packages.fetch(key_for(name, version)) do |key|
          packages[key] = PipPackage.new(name, version, PyPI.definition(name, version))
        end
        package.groups << group
      end
      packages.values
    end

    def lockfile_hash
      @lockfile_hash ||= JSON.parse(IO.read(detected_package_path))
    end
  end
end
