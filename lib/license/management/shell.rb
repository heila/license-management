# frozen_string_literal: true

module License
  module Management
    class Shell
      SPLIT_SCRIPT = "'BEGIN {x=0;} /BEGIN CERT/{x++} { print > \"custom.\" x \".crt\" }'"
      attr_reader :custom_certificate_path, :logger

      def initialize(logger: License::Management.logger, certificate: ENV['ADDITIONAL_CA_CERT_BUNDLE'])
        @logger = logger
        @custom_certificate_path = Pathname.new('/usr/local/share/ca-certificates/custom.crt')
        trust!(certificate) if present?(certificate)
      end

      def execute(command, env: {})
        expanded_command = expand(command)
        logger.debug(expanded_command)

        stdout, stderr, status = Open3.capture3(env, expanded_command)

        record(stdout, stderr)
        [stdout, stderr, status]
      end

      def sh(command, env: {})
        execute("sh -c '#{expand(command)}'", env: env)
      end

      def custom_certificate_installed?
        present?(ENV['ADDITIONAL_CA_CERT_BUNDLE']) && custom_certificate_path.exist?
      end

      private

      def expand(command)
        Array(command).flatten.map(&:to_s).join(' ')
      end

      def trust!(certificate)
        custom_certificate_path.write(certificate)
        Dir.chdir custom_certificate_path.dirname do
          execute([:awk, SPLIT_SCRIPT, '<', custom_certificate_path])
          execute('update-ca-certificates -v')

          Dir.glob('custom.*.crt').each do |path|
            full_path = File.expand_path(path)
            execute([:openssl, :x509, '-in', full_path, '-text', '-noout'])
            execute(keytool_import_command(full_path))
            execute(keytool_list_command)
          end
        end
      end

      def keytool_import_command(file_path)
        [
          :keytool,
          '-importcert',
          '-alias', Time.now.to_i,
          '-file', file_path,
          '-trustcacerts',
          '-noprompt',
          '-storepass', 'changeit',
          '-keystore', keystore_path
        ]
      end

      def keytool_list_command
        [:keytool, '-list', '-v', '-storepass changeit', '-keystore', keystore_path]
      end

      def keystore_path
        "#{ENV['JAVA_HOME']}/jre/lib/security/cacerts"
      end

      def present?(item)
        !item.nil? && !item.empty?
      end

      def record(stdout, stderr)
        logger.debug(stdout) if present?(stdout)
        logger.error(stderr) if present?(stderr)
      end
    end
  end
end
