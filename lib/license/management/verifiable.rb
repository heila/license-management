# frozen_string_literal: true

module License
  module Management
    module Verifiable
      def blank?(item)
        item.nil? || item.empty?
      end

      def present?(item)
        !blank?(item)
      end
    end
  end
end
