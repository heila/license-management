# frozen_string_literal: true

module License
  module Management
    class Python
      attr_reader :shell

      def initialize(shell: ::License::Management.shell)
        @shell = shell
      end

      def major_version
        version.split('.')[0]
      end

      def version
        ENV.fetch('ASDF_PYTHON_VERSION') do
          _stdout, stderr, status = shell.execute([:python, '--version'])
          status.success? ? stderr.split(' ')[-1] : 3
        end
      end

      def pip_index_url
        ENV.fetch('PIP_INDEX_URL', 'https://pypi.org/simple/')
      end

      def pip_licenses(venv: '.venv', detection_path:)
        _stdout, _stderr, status = shell.sh([
          ". #{venv}/bin/activate &&",
          :pip, :install,
          '--no-index',
          '--find-links $HOME/.config/virtualenv/app-data', 'pip-licenses', '&&',
          'pip-licenses',
          '--ignore-packages prettytable',
          '--with-description',
          '--with-urls',
          '--from=meta',
          '--format=json',
          '--output-file pip-licenses.json'
        ], env: { 'PATH' => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' })
        return [] unless status.success?

        JSON.parse(IO.read('pip-licenses.json')).map do |dependency|
          ::LicenseFinder::Dependency.new(
            'Pip',
            dependency['Name'],
            dependency['Version'],
            description: dependency['Description'],
            detection_path: detection_path,
            homepage: dependency['URL'],
            spec_licenses: [dependency['License']]
          )
        end
      end

      def default_env
        return {} unless shell.custom_certificate_installed?

        { 'PIP_CERT' => ENV.fetch('PIP_CERT', shell.custom_certificate_path.to_s) }
      end
    end
  end
end
